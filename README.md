# Argent bank project

## Prerequisites for install
- You need [Git](https://git-scm.com) to clone the repository
- You need [Node](https://nodejs.org/en/) (v16.10.0) to run the npm commands
- You need [MongoDB Community Server](https://www.mongodb.com/try/download/community) for the database

```bash
# Check Node.js version
node --version

# Check Mongo version
mongo --version
```

## Dependencies
- [React](https://reactjs.org): v17.0.2
- [React-router-dom](https://reactrouter.com/web/guides/quick-start): v5.3.0   
`$ npm install react-router-dom`   
- [Prop-types](https://www.npmjs.com/package/prop-types): v15.7.2  
`$ npm i prop-types`   
- [Axios](https://github.com/axios/axios): v0.21.4    
`$ npm install axios`   
- [@redux.js/toolkit](https://redux-toolkit.js.org/introduction/getting-started): v1.6.1   
`$ npm install @reduxjs/toolkit`    
- [react-redux](https://react-redux.js.org/introduction/getting-started): v7.2.5   
`$ npm install react-redux`   
- [redux-devtools-extension](https://github.com/zalmoxisus/redux-devtools-extension): v2.13.9   
`$ npm install --save redux-devtools-extension`   
- [swagger-ui-react](https://www.npmjs.com/package/swagger-ui-react): v3.52.3    
`$ npm i --save swagger-ui-react`   
- Recommended text editor: [Visual Studio Code](https://code.visualstudio.com)    

## Installing and launching Back-End
[ArgentBank Back-End Repository](https://github.com/zalmoxisus/redux-devtools-extension)

1. Clone the repository of ArgentBank back-end:   
`git clone https://github.com/OpenClassrooms-Student-Center/Project-10-Bank-API.git`  

2. Inside this back-end repository, install dependencies:  
`npm install`    

3. Start local dev server:   
`npm run dev:server`  

4. Populate database with two users:   
`npm run populate-db`  

5. Your server should now be running at http://locahost:3001 and you will now have two users in your MongoDB database  

## Installing and launching Front-End    
1. Clone the repository of ArgentBank front-end:   
`git clone https://gitlab.com/ptisky/MorganROY_13_15122021.git`    

2. Inside this front-end repository, install dependencies:   
`npm install`   

3. Launch front-end on port 3000:    
`npm start`    

4. Front-end is now rendered at URL `http://localhost:3000`.    
